#standart ruby Array#shuffle
puts (1..8).to_a.shuffle.join(',')

#custom ruby shuffle
class Array
  def fisher_yates
    n = length
    while n > 0
      i = rand(n -= 1)
      self[i], self[n] = self[n], self[i]
    end
    self
  end
end
puts (1..8).to_a.fisher_yates.join(',')