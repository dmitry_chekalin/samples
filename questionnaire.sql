# Data structure

CREATE TABLE  `questionnaire` (
id INT(11) NOT NULL AUTO_INCREMENT,
name VARCHAR(255) NOT NULL,
PRIMARY KEY (id)
);

CREATE TABLE `questions` (
id INT(11) NOT NULL AUTO_INCREMENT,
questionnaire_id INT(11) NOT NULL,
question TEXT,
ordernum SMALLINT NOT NULL,
score INT NOT NULL DEFAULT 0,
PRIMARY KEY (id)
);

CREATE TABLE `answers` (
id INT(11) NOT NULL AUTO_INCREMENT,
question_id  INT(11) NOT NULL,
user_id  INT(11) NOT NULL,
answer TEXT,
PRIMARY KEY (id)
);

CREATE TABLE `users` (
id INT(11) NOT NULL AUTO_INCREMENT,
first_name VARCHAR(255) NOT NULL,
last_name VARCHAR(255),
PRIMARY KEY (id)
);

# SELECT

SELECT a.answer
FROM answers a
INNER JOIN users u ON a.user_id = u.id
INNER JOIN questions q ON a.question_id = q.id
ORDER BY q.ordernum
